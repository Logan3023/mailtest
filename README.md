Imported files are expected to have header columns in the first row named: name, email, postalcode.

Jobs are queued on the default queue.

I did not spend extra time to cleanup meticulously. It works and I believe it meets your mockup criteria.

Thank you.