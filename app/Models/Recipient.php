<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\Importable;

class Recipient extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'recipients';

    public function mailingList()
    {
        return $this->belongsTo(MailingList::class, 'id', 'list_id');
    }
}
