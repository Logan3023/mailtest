<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MailingList extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'mailing_lists';


    public function recipients()
    {
        return $this->hasMany(Recipient::class, 'list_id', 'id');
    }
}
