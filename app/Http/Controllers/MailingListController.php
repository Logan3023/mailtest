<?php

namespace App\Http\Controllers;

use App\Models\MailingList;
use App\Models\Recipient;
use Illuminate\Http\Request;

class MailingListController extends Controller
{
    public function index()
    {
        $lists = MailingList::all();

        return $lists;
    }

    public function show($id)
    {

//        return Recipient::first();
//        dd('test');
        $list = MailingList::find( $id);
        $recipients = Recipient::where('list_id', $id)->get();

        return view('mailing-list-details', ['list' => $list, 'details' => $recipients]);
    }
}
