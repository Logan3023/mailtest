<?php

namespace App\Http\Controllers;

use App\Models\MailingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\RecipientImport;
use App\Exports\RecipientExport;

class RecipientController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function fileImportExport()
    {
        return view('file-import');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function fileImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'list_name' => 'required|string|unique:mailing_lists,name|max:55|min:5',
            'file'      => 'required',
            'date'      => 'required|date',
        ]);
        if ($validator->fails()) {
            return response($validator->errors(), 401);
        }

        $list = MailingList::create([
            'name' => $request->list_name,
            'date' => $request->date
        ]);
        Excel::queueImport(new RecipientImport, $request->file('file')->store('mailinglists'));

        return response()->json(['newlist' => $list]);
    }


    public function fileExport($id)
    {
        $list = MailingList::find($id);

       return (new RecipientExport($list->id))->download('recipients.xlsx');
       
    }
}
