<?php

namespace App\Imports;
use App\Models\MailingList;
use App\Models\Recipient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RecipientImport implements ToModel, WithChunkReading, ShouldQueue, WithHeadingRow, SkipsOnError
{
    use Importable, SkipsErrors;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $list = MailingList::latest()->first();

        return new Recipient([
            'name'        => $row['name'],
            'email'       => $row['email'],
            'postal_code' => $row['postalcode'],
            'list_id'     => $list->id
        ]);
    }

    public function chunkSize(): int
    {
        return 10000;
    }
    public function batchSize(): int
    {
        return 10000;
    }
}
