<?php


use App\Http\Controllers\MailingListController;
use App\Http\Controllers\RecipientController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [RecipientController::class, 'fileImportExport'])->name('create');


//Route::get('file-import-export', [RecipientController::class, 'fileImportExport']);
Route::post('file-import', [RecipientController::class, 'fileImport'])->name('file-import');
Route::get('file-export/{id}', [RecipientController::class, 'fileExport'])->name('file-export');

Route::group(['prefix' => 'mail'], function () {
    Route::get('details/{id}', [MailingListController::class, 'show']);
    Route::get('/create', [RecipientController::class, 'fileImportExport'])->name('create');
    Route::get('/lists', [App\Http\Controllers\MailingListController::class, 'index']);
});

